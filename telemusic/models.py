from tortoise.models import Model
from tortoise import fields


class Chat(Model):
    id = fields.IntField(pk=True)
    songs = fields.ManyToManyField("models.Song", related_name="queue")
    skipping = fields.ManyToManyField("models.User", related_name="skipping")
    loop = fields.BooleanField(default=False)


class Song(Model):
    title = fields.CharField(max_length=256)
    url = fields.CharField(max_length=256)
    duration = fields.IntField(default=0)

    requested_by = fields.ForeignKeyField("models.User")

    def __str__(self):
        return f"<{type(self).__name__}: {self.title}>"

    def __repr__(self):
        return str(self)


class User(Model):
    id = fields.IntField(pk=True)
    username = fields.CharField(max_length=32, null=True)
