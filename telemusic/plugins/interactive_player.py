import logging

from pyrogram.types import Message, CallbackQuery
from pyrogram.filters import command, regex

from ..helpers import bot_connected_only, connected_users_only, localise
from ..constants import (SEEK_VALUE,
                         VOLUME_VALUE, MAX_VOLUME)
from ..bot import Telemusic, localiser as _

log = logging.getLogger(__name__)


@Telemusic.on_message(command("player"))
@localise
@bot_connected_only()
@connected_users_only()
async def player(client, message: Message):
    song, now = await client.get_current_song(message.chat.id)
    if not song:
        await message.reply_text(_("nothing_playing"))
        return

    voice_chat = client.get_voice_chat(message.chat.id)
    if voice_chat.player_msg:
        await voice_chat.player_msg.delete()
    voice_chat.player_msg = await message.reply_text("...")
    voice_chat.update_player()


@Telemusic.on_callback_query(regex(r"(D?)(.*)ward:([-\d]+)"))
async def seek(client, query: CallbackQuery):
    match = query.matches[0]

    chat_id = int(match.group(3))

    voice_chat = client.get_voice_chat(chat_id)

    k = 2 if match.group(1) else 1
    k *= -1 if match.group(2) == "back" else 1

    voice_chat.seek(SEEK_VALUE * k)
    voice_chat.update_player()


@Telemusic.on_callback_query(regex(r"vol_(up|down):([-\d]+)"))
async def volume(client, query: CallbackQuery):
    match = query.matches[0]

    chat_id = int(match.group(2))
    up = match.group(1) == "up"

    voice_chat = client.get_voice_chat(chat_id)
    if up:
        if voice_chat.volume + VOLUME_VALUE > MAX_VOLUME:
            await query.answer(_("evil_volume"))
        else:
            voice_chat.volume_up(VOLUME_VALUE)
    else:
        voice_chat.volume_down(VOLUME_VALUE)
    voice_chat.update_player()


@Telemusic.on_callback_query(regex(r"(pause|resume):([-\d]+)"))
async def pause(client, query: CallbackQuery):
    match = query.matches[0]
    chat_id = int(match.group(2))

    voice_chat = client.get_voice_chat(chat_id)
    pause = match.group(1) == "pause"
    if pause:
        voice_chat.pause()
        await voice_chat._update_player()
    else:
        voice_chat.resume()
        voice_chat.update_player()
