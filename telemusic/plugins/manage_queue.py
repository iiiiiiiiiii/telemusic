from pyrogram.filters import command

from ..helpers import (localise,
                       bot_connected_only, connected_users_only,
                       format_song)
from ..bot import Telemusic, localiser as _


@Telemusic.on_message(command("remove"))
@localise
@bot_connected_only()
@connected_users_only()
async def remove(client, message):
    chat_id = message.chat.id
    remove_index = int(message.command[1]) if len(
        message.command) > 1 and message.command[1].isdecimal() else 0

    if remove_index < 1:
        await message.reply_text(_("wrong_argument"))
        return

    song = await client.remove_song(chat_id, remove_index)
    if song:
        await message.reply_text(_("removed_song", song=format_song(song)))
    else:
        await message.reply_text(_("no_song"))
