import logging

from pyrogram.filters import command

from ..helpers import (
    bot_connected_only,
    connected_users_only,
    format_seconds,
    localise)
from ..constants import MAX_VOLUME
from ..bot import Telemusic, localiser as _

log = logging.getLogger(__name__)


@Telemusic.on_message(command("volume"))
@localise
@bot_connected_only()
@connected_users_only()
async def volume(client, message):
    cmd = message.command
    if len(cmd) > 1 and cmd[1].strip("+-").isdecimal():
        new_volume = int(cmd[1])
    else:
        new_volume = 0

    vc = client.get_voice_chat(message.chat.id)
    if new_volume:
        volume_up = message.command[1].startswith("+")
        if volume_up or new_volume < 0:
            new_volume = vc.volume + new_volume
        if new_volume > MAX_VOLUME or new_volume < 0:
            await message.reply_text(_("wrong_argument"))
        else:
            vc.set_volume(new_volume)
            await message.reply_text(_("set_volume", volume=int(new_volume)))
    else:
        await message.reply_text(_("current_volume", volume=int(vc.volume)))


@Telemusic.on_message(command("seek"))
@localise
@bot_connected_only()
@connected_users_only()
async def seek(client, message):
    if await client.is_queue_empty(message.chat.id):
        await message.reply_text(_("nothing_playing"))

    cmd = message.command
    if len(cmd) > 1 and cmd[1].strip("+-").isdecimal():
        seek_secs = int(cmd[1])
    else:
        seek_secs = 0

    vc = client.get_voice_chat(message.chat.id)
    if seek_secs:
        ok = vc.seek(seek_secs)
        if ok is not None:
            direction = 'forward' if seek_secs > 0 else 'backward'
            await message.reply_text(_("seek_" + direction,
                                       seek=_("seconds", count=abs(seek_secs)),
                                       now=format_seconds(vc.time_pos)))
        else:
            await message.reply_text(_("unsuccessful_seek"))
    else:
        await message.reply_text(_("wrong_argument"))


@Telemusic.on_message(command("resume") | command("pause"))
@localise
@bot_connected_only()
@connected_users_only()
async def resume(client, message):
    vc = client.get_voice_chat(message.chat.id)
    pause = message.command[0] == "pause"
    if pause:
        vc.pause()
    else:
        vc.resume()
    await message.reply_text(_("paused") if pause else _("resumed"))
