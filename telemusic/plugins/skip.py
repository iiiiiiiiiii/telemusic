from math import ceil

from pyrogram.filters import command

from ..bot import Telemusic, localiser as _
from ..helpers import (localise,
                       bot_connected_only, connected_users_only)


@Telemusic.on_message(command("skip"))
@localise
@bot_connected_only()
@connected_users_only()
async def skip(client, message):
    chat_id = message.chat.id
    user_id = message.from_user.id
    if await client.is_queue_empty(chat_id):
        await message.reply_text(_("nothing_playing"))
    else:
        if await client.is_skipping(chat_id, user_id):
            await message.reply_text(_("already_voted"))
            return
        skippers = await client.count_skippers(chat_id)
        call_participants = message.chat.call.participants_count
        need_votes = ceil(call_participants / 2)

        if skippers + 1 >= need_votes:
            await client.get_voice_chat(chat_id).skip()
            await client.clear_skip_vote(chat_id)
            await message.reply_text(_("skipped"))
        else:
            await client.add_skip_vote(chat_id, user_id)
            await message.reply_text(_("voted",
                                       voted=skippers + 1, needed=need_votes))


@Telemusic.on_message(command("unskip"))
@localise
@bot_connected_only()
@connected_users_only()
async def unskip(client, message):
    chat_id = message.chat.id
    user_id = message.from_user.id
    if await client.is_queue_empty(chat_id):
        await message.reply_text(_("nothing_playing"))
    else:
        if not await client.is_skipping(chat_id, user_id):
            await message.reply_text(_("not_voted"))
            return
        skippers = await client.count_skippers(chat_id)
        call_participants = message.chat.call.participants_count
        need_votes = ceil(call_participants / 2)

        await client.delete_skip_vote(chat_id, user_id)
        await message.reply_text(_("voted",
                                   voted=skippers - 1, needed=need_votes))
