from pyrogram.filters import command

from ..bot import Telemusic, localiser as _
from ..helpers import (localise,
                       bot_connected_only, connected_users_only,
                       format_now_playing, format_song)


@Telemusic.on_message(command("clear"))
@localise
@bot_connected_only()
@connected_users_only()
async def clear(client, message):
    await client.clear_queue(message.chat.id)
    await message.reply_text(_("cleared_queue"))


@Telemusic.on_message(command("queue"))
@localise
@bot_connected_only()
async def queue(client, message):
    if await client.is_queue_empty(message.chat.id):
        await message.reply_text(_("nothing_playing"))
        return

    songs, total = await client.get_queue(message.chat.id)

    song = songs[0]
    text = _("now_playing", song=format_song(song, True))

    if songs[1:]:
        text += _("queue")

        for i, song in enumerate(songs[1:], start=1):
            text += f"{i}. {format_song(song, True)}\n"
        text += f"\n`[{len(songs[1:])}/{total-1}]`"
    else:
        text += _("empty_queue")

    await message.reply_text(text, disable_web_page_preview=True)


@Telemusic.on_message(command(["now_playing", "np"]))
@localise
@bot_connected_only()
async def now_playing(client, message):
    song, place = await client.get_current_song(message.chat.id)
    await message.reply_text(await format_now_playing(song,
                                                      place),
                             disable_web_page_preview=True)
