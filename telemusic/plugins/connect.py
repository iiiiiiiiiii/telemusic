from pyrogram.filters import command

from ..helpers import localise, bot_connected_only, connected_users_only
from ..bot import Telemusic, localiser as _
from ..exceptions import UserBotNoChatAccessException


@Telemusic.on_message(command("join"))
@localise
@connected_users_only()
async def join(client, message):
    if client.is_call_connected(message.chat.id):
        await message.reply_text(_("already_connected"))
        return
    try:
        ok = await client.connect_voice(message.chat.id)
    except UserBotNoChatAccessException:
        await message.reply_text(_("userbot_no_chat_access"))
        return

    if not ok:
        await message.reply_text(_("vc_not_started"))


@Telemusic.on_message(command("stop"))
@localise
@bot_connected_only()
@connected_users_only()
async def stop(client, message):
    await client.stop_call(message.chat.id)
    await message.reply_text(_("disconnected"))
