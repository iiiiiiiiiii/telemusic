from pyrogram.filters import command
from youtube_dl.utils import ExtractorError

from ..models import Song, User
from ..bot import Telemusic, localiser as _
from ..helpers import (bot_connected_only, connected_users_only,
                       localise,
                       get_videos, format_song)


@Telemusic.on_message(command("play"))
@localise
@bot_connected_only(connect=True)
@connected_users_only()
async def play(client, message):
    req = " ".join(message.command[1:])
    try:
        videos = await get_videos(req)
    except ExtractorError as e:
        await message.reply_text(_("youtube_error", error='. '.join(e.args)))
        return

    if not videos:
        await message.reply_text(_("not_found"))
        return

    is_playlist = len(videos) > 1

    if is_playlist:
        msg = await message.reply_text(_("adding_playlist"))

    user, created = await User.get_or_create(id=message.from_user.id)
    if user.username != message.from_user.username:
        user.username = message.from_user.username or "user"
        await user.save()

    is_queue_empty = await client.is_queue_empty(message.chat.id)
    count = 0
    for video in videos:
        if not video:
            continue
        count += 1

        song = await Song.create(title=video.title, url=video.url, duration=video.duration or 0,
                                 requested_by=user)

        await client.play(message.chat.id, song)
        if not is_queue_empty and not is_playlist:
            await message.reply_text(_("added_song", song=format_song(song)))

    if is_playlist:
        await msg.delete()
        await message.reply_text(_("added_videos", count=count))
