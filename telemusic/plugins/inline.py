import logging

from pyrogram.types import InlineQueryResultVideo, InputTextMessageContent, InlineQueryResultPhoto

from ..bot import Telemusic
from ..helpers import search_videos

CACHE_TIME = 0
log = logging.getLogger(__name__)


@Telemusic.on_inline_query()
async def inline(client, query):
    string = query.query.strip()
    if not string:
        await query.answer(
                results=[],
                cache_time=CACHE_TIME
            )
        return

    videos = await search_videos(string)
    results = [
                InlineQueryResultVideo(
                    video_url=vid.embed_url,
                    title=vid.title,
                    thumb_url=vid.thumb_url,
                    mime_type="text/html",
                    description=vid.uploader,
                    id=str(vid),
                    input_message_content=InputTextMessageContent(
                        f"/play {vid.url}")
                    )
                for vid in videos
            ]

    await query.answer(
        results=results,
        cache_time=CACHE_TIME
    )
