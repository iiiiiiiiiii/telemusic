import time
import asyncio
import logging
from io import StringIO
import traceback as tb
from contextlib import redirect_stdout, redirect_stderr

from pyrogram.filters import command
from pyrogram.types import Message

from ..bot import Telemusic
from ..helpers import admins_only, wrap_code
from ..constants import EMPTY_SYMBOL, EXEC_TIMEOUT, PING_SOUND_PATH


log = logging.getLogger(__name__)


@Telemusic.on_message(command("ping"))
async def ping(client, message: Message):
    chat_id = message.chat.id
    start = time.time()
    reply = await message.reply_text("...")

    # :DDDDD
    if client.is_call_connected(chat_id):
        vc = client.get_voice_chat(chat_id)
        asyncio.create_task(vc.play_in_parallel(PING_SOUND_PATH))

    delta_ping = (time.time() - start) * 1000

    await reply.edit_text(f"**Pong!** `{int(delta_ping)} ms`")


async def coro_exec(coro, message, query, query_text="query"):
    text = f"<b>{query_text}</b> {wrap_code(query)}{EMPTY_SYMBOL}\n"
    if message.outgoing:
        await message.edit_text(text + "<i>running..</i>", parse_mode="html")
    else:
        message = await message.reply_text(text + "<i>running..</i>",
                                           parse_mode="html")
    end_text = ""
    sout, serr, result = StringIO(), StringIO(), None

    try:
        with redirect_stdout(sout), redirect_stderr(serr):
            result = await asyncio.wait_for(coro, timeout=EXEC_TIMEOUT)

    except asyncio.TimeoutError:
        end_text += f"<b>{EXEC_TIMEOUT} seconds timeout</b>"
    except Exception as err:
        end_text += ("<b>error</b>:\n" +
                     wrap_code(tb.format_exc(None, err)))
    finally:
        sout = sout.getvalue().strip()
        serr = serr.getvalue().strip()
        result = str(result)
        if sout:
            text += ("<b>stdout</b>:\n"
                     f"{wrap_code(sout)}\n\n")
        if serr:
            text += ("<b>stderr</b>:\n"
                     f"{wrap_code(serr)}\n\n")
        if result:
            text += ("<b>result</b>:\n"
                     f"{wrap_code(result)}\n\n")
        text += end_text
        if not end_text and not (serr or sout or result):
            text += "<b>completed successfuly</b>"

    await message.edit_text(text, parse_mode="html")


async def _pyexec(client, message, code):
    _code = "async def __exec():\n " + '\n '.join(code.splitlines())
    g = globals()
    g.update(locals())
    exec(_code, g)
    return await __exec()


@Telemusic.on_message(command("e", "/"))
@admins_only
async def python_exec_cmd(client: Telemusic, message):
    code = message.text[len(message.command[0]) +
                        1:].strip()  # +1 means prefix
    await coro_exec(_pyexec(client, message, code), message,
                    code, "python code")
