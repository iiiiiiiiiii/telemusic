import logging
from pyrogram.raw.types import UpdateGroupCall, UpdateGroupCallParticipants

from ..bot import Telemusic

log = logging.getLogger(__name__)


@Telemusic.on_raw_update(group=-1)
async def group_call(client, update, user, chats):
    if not isinstance(update, (UpdateGroupCall, UpdateGroupCallParticipants)):
        return
    log.debug(f"received {update}")
