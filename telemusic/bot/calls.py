import logging
import asyncio
from tgcalls import GroupCallService

from ..types.chat import Chat
from ..constants import CONNECT_SOUND_PATH, DISCONNECT_SOUND_PATH
from ..exceptions import UserBotNoChatAccessException
from pyrogram.errors import PeerIdInvalid, ChannelPrivate
from .localisation import localiser as _

log = logging.getLogger(__name__)


class CallMethods:
    async def process_call(self, chat_id):
        await self.send_message(chat_id, _("connected_to_vc"))
        chat = self.chats[chat_id]
        await chat.play_in_parallel(CONNECT_SOUND_PATH)
        call = chat.call
        while call.connected:
            if chat.player_msg:
                await chat.player_msg.delete()
            chat.player_msg = None
            while await self.is_queue_empty(chat_id):
                await asyncio.sleep(1)

            song = await self.get_song(chat_id)

            chat.song = song
            chat.play(song.url)

            chat.player_msg = await self.send_message(chat_id, "...")
            chat.update_player()

            await chat.wait()
            chat.song = None

            await self.next_song(chat_id)
            await asyncio.sleep(0.1)

    async def get_call(self, chat_id):
        chat = self.get_voice_chat(chat_id)

        return await self.user_bot.get_call(chat.input_call)

    async def leave_group_call(self, call, source):
        return await self.user_bot.leave_group_call(call, source)

    async def join_group_call(self, call, params, muted):
        return await self.user_bot.join_group_call(call, params, muted)

    async def connect_voice(self, chat_id):
        try:
            chat = await self.user_bot.get_full_chat(chat_id)
        except (PeerIdInvalid, ChannelPrivate):
            raise UserBotNoChatAccessException() from None

        if not chat.call:
            return False

        call = chat.call
        gcall = GroupCallService(call.id, call.access_hash)
        self.chats[chat_id] = vc = Chat(
            chat_id,
            call=gcall, input_call=call, audio_device=self.audio_device)

        @gcall.on_start()
        async def _():
            await self.process_call(chat_id)

        @gcall.on_stop()
        async def _():
            await self.on_call_stop(chat_id)

        self.loop.create_task(gcall.start())
        self.loop.create_task(vc._loop())
        return await self.user_bot.connect_voice(vc)

    async def on_call_stop(self, chat_id):
        chat = self.chats.pop(chat_id)
        await chat.play_in_parallel(DISCONNECT_SOUND_PATH)
        try:
            await self.leave_group_call(chat.input_call, chat.call.ssrc)
        except Exception as e:
            log.error(e, exc_info=True)

    async def stop_call(self, chat_id):
        chat = self.chats[chat_id]
        await chat.stop()

    def get_voice_chat(self, chat_id):
        return self.chats.get(chat_id)

    def is_call_connected(self, chat_id):
        chat = self.get_voice_chat(chat_id)
        return chat.call.connected if chat else None
