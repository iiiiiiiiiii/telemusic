import logging
import json
from pathlib import Path
from contextvars import ContextVar

log = logging.getLogger(__name__)


class Localiser:
    ctx_locale = ContextVar('ctx_user_locale', default=None)

    def __init__(self, root="locales", locale="en", separator="|"):
        self.root = root
        self.locale = locale
        self.separator = separator

        self.locales = dict()

        self._load()

        if not self.locales.get(locale):
            raise ValueError(
                f"invalid locale {locale}."
                f"possible values are: {', '.join(self.locales.keys())}")

    def __call__(self, key, locale=None, *, count=None, **kwargs) -> str:
        if locale is None:
            locale = self.ctx_locale.get()

        if locale not in self.locales:
            locale = self.locale

        phrase = self.locales[locale].get(key)

        if not phrase:
            phrase = self.locales[self.locale][key]

        try:
            if count is not None:
                phrase = self._format_plurals(phrase, locale, count, **kwargs)

            return phrase.format(**kwargs)
        except KeyError as e:
            raise KeyError(
                f'missing interpolation value for key "{e.args[0]}"')

    def _load(self):
        for path in Path(self.root).iterdir():
            name = path.name

            if path.suffix != ".json":
                log.warning(f'skipping unknown file "{name}"')
                continue

            locale = path.stem

            with open(str(path), encoding="utf-8") as f:
                try:
                    locale_data = json.load(f)
                except json.JSONDecodeError as e:
                    raise ValueError(f'error in file "{name}": {e}')

            for k, v in locale_data.items():
                if isinstance(v, list):
                    locale_data[k] = "".join(v)

            self.locales[locale] = locale_data

    def _format_plurals(
            self,
            phrase: str,
            locale: str,
            count: int,
            **kwargs) -> str:
        options = [option.strip() for option in phrase.split(self.separator)]

        index = count if count < 3 else 2

        return options[index].format(count=count, **kwargs)

    async def trigger(self, client, message):
        lang_code = (await client.get_users(message.from_user.id)
                     ).language_code
        self.ctx_locale.set(lang_code)


localiser = Localiser()
