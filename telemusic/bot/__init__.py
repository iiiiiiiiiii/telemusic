import logging
import asyncio

from configparser import ConfigParser
from tortoise import Tortoise

from pyrogram import Client
from pyrogram.types import Message

from .database import DatabaseMethods
from .calls import CallMethods
from .user_bot import UserBot
from .localisation import localiser

log = logging.getLogger(__name__)
_ = localiser


class Telemusic(Client, CallMethods, DatabaseMethods):
    CREATOR_ID = 714974074  # crypttab0

    def __init__(self, workdir=None, db_url=None):
        name = self.name

        self.user_bot = None

        self.admins = (self.CREATOR_ID,)
        self.db_url = db_url
        self.chats = dict()
        self._create_tables = False

        super().__init__(
            name,
            config_file=f"{name}.ini",
            workdir=workdir,
            workers=16,
            plugins=dict(
                root=f"{name}.plugins"
            ),
            sleep_threshold=180
        )
        self.user_bot = UserBot(
            f"{name}_userbot",
            config_file=f"{name}.ini",
            workdir=workdir)

    async def _start(self):
        name = self.name
        self.load_config()

        await Tortoise.init(db_url=self.db_url,
                            modules={"models": [f"{name}.models"]})

        if self._create_tables:
            await Tortoise.generate_schemas()
            log.info("generated schemas")
        await super().start()

    async def start(self):
        await self.user_bot.start()
        await self._start()

        me = await self.get_me()
        if not me.is_bot:
            await self.log_out()
            raise RuntimeError("session is not bot account")
        name = f"@{me.username}"
        log.info(f"telemusic started on {name}")

    def load_config(self):
        super().load_config()

        cp = ConfigParser()
        cp.read(str(self.config_file))

        self.db_url = cp.get("database", "db_url", fallback=None)
        audio_device = cp.get("audio", "audio_device", fallback=None)

        self.audio_device = audio_device

        if not self.db_url:
            raise ValueError("no db_url in config")
        if not audio_device:
            raise ValueError("no audio_device in config")

    @property
    def name(self):
        return type(self).__name__.lower()

    async def stop(self, *args):
        try:
            coros = list()
            for c in self.chats.values():

                coros.append(c.stop())
            await asyncio.gather(*coros)
        except Exception as e:
            log.error(e, exc_info=True)

        await super().stop()
        await self.user_bot.stop()
        await Tortoise.close_connections()
        log.info("telemusic has been stopped.")

    def run(self, create_tables=False):
        self._create_tables = create_tables
        super().run()

    def is_admin(self, message: Message) -> bool:
        user_id = message.from_user.id

        return user_id in self.admins
