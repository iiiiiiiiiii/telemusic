import logging

from pyrogram import Client, raw
from pyrogram.syncer import Syncer
from pyrogram.raw.functions.phone import (
    GetGroupCall, LeaveGroupCall, JoinGroupCall)

log = logging.getLogger(__name__)


class UserBot(Client):
    async def connect_voice(self, chat):
        if not chat.call:
            return False

        # call = (await self.send(GetGroupCall(call=chat.call))).call
        gcall = chat.call
        # params = to_data_json(await gcall.join())

        # resp = (await self.join_group_call(call, params, False)).call
        # new_params = json.loads(resp.params.data)

        await gcall.connect({
                                "transport": {
                                    "fingerprints": [],
                                    "candidates": [],
                                    "pwd": "",
                                    "ufrag": ""
                                }
                            })
        return True

    async def join_group_call(self, call, params, muted):
        resp = await self.send(JoinGroupCall(call=call,
                                             params=params, muted=muted))
        updates = resp.updates
        return updates[0] if updates else None

    async def leave_group_call(self, call, source):
        resp = await self.send(LeaveGroupCall(call=call, source=source))
        updates = resp.updates
        return updates[0] if updates else None

    async def get_call(self, call):
        return (await self.send(GetGroupCall(call=call)))

    async def get_full_chat(self, chat):
        if isinstance(chat, (int, str)):
            peer = await self.resolve_peer(chat)
        else:
            peer = chat

        if isinstance(peer, raw.types.InputPeerChannel):
            r = await self.send(
                    raw.functions.channels.GetFullChannel(channel=peer))
        elif isinstance(peer, (raw.types.InputPeerUser,
                               raw.types.InputPeerSelf)):
            r = await self.send(raw.functions.users.GetFullUser(id=peer))
        else:
            r = await self.send(
                    raw.functions.messages.GetFullChat(chat_id=peer.chat_id))
        return r.full_chat

    async def stop(self, *args):
        await super().stop()
        log.info("telemusic userbot has been stopped.")

    async def _start(self):
        is_authorized = await self.connect()
        self.bot_token = None

        try:
            if not is_authorized:
                await self.authorize()
        except (Exception, KeyboardInterrupt):
            await self.disconnect()
            raise
        else:
            self.is_initialized = True
            await Syncer.add(self)
            return self

    async def start(self):
        await self._start()
        me = await self.get_me()
        if me.is_bot:
            await self.log_out()
            raise RuntimeError("session is not user account")
        name = f"@{me.username}" if me.username else f"{me.first_name}"
        log.info(f"telemusic userbot started on {name}")
