import json

from pyrogram.raw.types import DataJSON


def to_data_json(d):
    return DataJSON(data=json.dumps(d))
