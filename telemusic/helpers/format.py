import html
from ..bot.localisation import localiser
_ = localiser


def wrap_code(text):
    return f"<pre>{html.escape(text)}</pre>"


def mention_user(user):
    return f"[{user.username}](tg://user?id={user.id})"


def format_song(song, show_duration=False):
    return f"[{song.title}]({song.url})" + \
        (f" `[{format_seconds(song.duration)}]`" if show_duration else "")


def format_seconds(secs):
    secs = int(secs or 0)
    days, r = divmod(secs, 86400)
    hours, r = divmod(r, 3600)
    minutes, seconds = divmod(r, 60)

    days_string = _("days", count=days) + " " if days else ""
    hours_string = f"{hours:0>2}:" if hours else ""
    minutes_string = f"{minutes:0>2}:"
    seconds_string = f"{seconds:0>2}"
    return days_string + hours_string + minutes_string + seconds_string


async def format_now_playing(song, now=0):
    if not song:
        return _("nothing_playing")
    else:
        req = await song.requested_by
        return _(
            "full_now_playing",
            song=format_song(song),
            now=format_seconds(now),
            duration=format_seconds(
                song.duration),
            user=mention_user(req) if req else "")


def fit_length(text, width, placeholder="-"):
    c = (width - len(text)) // 2
    if c < 0:
        c = 0
    return placeholder * c + text + placeholder * c
