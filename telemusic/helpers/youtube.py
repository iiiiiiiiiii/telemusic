import logging
import asyncio

from functools import partial

from youtube_dl import YoutubeDL as YDL
from youtube_dl.utils import DownloadError

from ..constants import YDL_OPTIONS
from ..types.videos import YoutubeVideo
log = logging.getLogger(__name__)
ydl = YDL(YDL_OPTIONS)


async def get_videos(req):
    loop = asyncio.get_event_loop()
    try:
        direct_link = req.startswith("https://")
        req = req if direct_link else f"ytsearch:{req}"

        extract = partial(ydl.extract_info, req, download=False)
        info = await loop.run_in_executor(None, extract)
        entries = info.get("entries") or [info]

        if not entries:
            log.debug(f"nothing was found for {req}")
            return None
    except DownloadError as e:
        if len(e.exc_info) >= 2:
            raise e.exc_info[1]
        else:
            log.debug(f"download error: {e}", exc_info=True)
            return None
    log.debug(f"found {len(entries)} entries for {req}")
    # return video["title"], video["formats"][0]["url"], video["duration"]
    log.debug(f"first entry: {entries[0]}")
    return [YoutubeVideo(**v)
            for v in entries if all(k in v for k in ("title", "id", "duration"))]

async def search_videos(text, lim=10):
    loop = asyncio.get_event_loop()
    
    extract = partial(ydl.extract_info, f"ytsearch{lim}:{text}", download=False)
    entries = (await loop.run_in_executor(None, extract))["entries"]
    return [YoutubeVideo(**v)
            for v in entries if all(k in v for k in ("title", "id", "duration"))]
