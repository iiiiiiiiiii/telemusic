from functools import wraps

from pyrogram.raw.types import UpdateGroupCall, UpdateGroupCallParticipants

from ..exceptions import UserBotNoChatAccessException
from ..bot.localisation import localiser
_ = localiser


def bot_connected_only(connect=False):
    def decorator(func):
        @wraps(func)
        async def wrapped(client, message):
            if client.is_call_connected(message.chat.id):
                await func(client, message)
            else:
                if not connect:
                    await message.reply_text(_("not_connected"))
                else:
                    try:
                        ok = await client.connect_voice(message.chat.id)
                    except UserBotNoChatAccessException:
                        await message.reply_text(_("userbot_no_chat_access"))
                        return

                    if ok:
                        await func(client, message)
                    else:
                        await message.reply_text(_("vc_not_started"))
        return wrapped

    return decorator


def connected_users_only():
    def decorator(func):
        @wraps(func)
        async def wrapped(client, message):
            user_id = message.from_user.id
            chat_id = message.chat.id

            call = await client.get_call(chat_id)
            message.chat.call = call.call
            if user_id not in [u.user_id for u in call.participants]:
                await message.reply_text(_("user_not_connected"))
            else:
                await func(client, message)
        return wrapped

    return decorator


def admins_only(func):
    @wraps(func)
    async def decorator(client, message):
        if client.is_admin(message):
            await func(client, message)
        await message.delete()
    decorator.admin = True

    return decorator


def localise(func):
    @wraps(func)
    async def decorator(client, message):
        await localiser.trigger(client, message)
        await func(client, message)

    return decorator


def group_call_update_filter(update):
    return isinstance(
        update, (UpdateGroupCall, UpdateGroupCallParticipants))
