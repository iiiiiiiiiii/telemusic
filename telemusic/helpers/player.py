from . import fit_length, format_song, format_seconds
from ..constants import (MAX_PLAYER_LENGTH, BAR_PLACEHOLDER, BAR_CURSOR,
                         BACKWARD, FORWARD, PAUSE, RESUME,
                         VOLUME_UP, VOLUME_DOWN, FAST_FORWARD, FAST_BACKWARD)
from ..types import InlineKeyboardMarkupBuilder, ActionBuilder


def create_player(voice_chat, chat_id):
    song = voice_chat.song
    text = (fit_length(format_song(song), MAX_PLAYER_LENGTH) +
            "\n```" +
            create_bar(voice_chat.time_pos, song.duration, MAX_PLAYER_LENGTH) +
            "```")
    keyboard = create_keyboard(voice_chat.time_pos, voice_chat.song.duration,
                               voice_chat.volume, voice_chat.is_paused,
                               chat_id)
    return text, keyboard


def create_bar(now, duration, width):
    perc = now / duration if duration else 0.5
    left_chars = int(perc * width)
    right_chars = int((1 - perc) * width)
    return (BAR_PLACEHOLDER * left_chars
            + BAR_CURSOR
            + BAR_PLACEHOLDER * right_chars)


def create_keyboard(now, duration, volume, paused, chat_id):
    time = f"{format_seconds(now)}/{format_seconds(duration)}"
    volume = f"{volume}%"

    action = ActionBuilder(chat_id)
    keyboard = InlineKeyboardMarkupBuilder()
    keyboard.add_button(BACKWARD, action.backward)\
            .add_button(RESUME if paused else PAUSE,
                        action.resume if paused else action.pause)\
            .add_button(FORWARD, action.forward)\
            .new_row()

    keyboard.add_button(FAST_BACKWARD, action.double_backward)\
            .add_button(time, action.none)\
            .add_button(FAST_FORWARD, action.double_forward)\
            .new_row()

    keyboard.add_button(VOLUME_DOWN, action.volume_down)\
            .add_button(volume, action.none)\
            .add_button(VOLUME_UP, action.volume_up)
    return keyboard
