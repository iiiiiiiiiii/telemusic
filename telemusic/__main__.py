import sys
import logging

from pathlib import Path
from argparse import ArgumentParser, BooleanOptionalAction

from .logger import ColoredFormatter
from .bot import Telemusic


if __name__ == "__main__":
    pyrogram_filter = lambda record: ("pyrogram" not in record.name
                                      or record.levelno > logging.INFO)
    aiosqlite_filter = lambda record: "aiosqlite" not in record.name
    tortoise_filter = lambda record: "db_client" not in record.name
    database_filter = lambda record: (not record.levelno <= logging.DEBUG
                                      or (aiosqlite_filter(record)
                                      and tortoise_filter(record)))

    h = logging.StreamHandler()
    h.setFormatter(ColoredFormatter())

    h.addFilter(pyrogram_filter)
    h.addFilter(database_filter)

    parser = ArgumentParser("telemusic bot")
    parser.add_argument("--create-tables", default=False,
                        help="create tables", action=BooleanOptionalAction)
    parser.add_argument("-v", "--verbose", default=False,
                        help="enable debug", action=BooleanOptionalAction)

    args = parser.parse_args()
    logging.basicConfig(handlers=[h],
                        level=logging.DEBUG if args.verbose else logging.INFO)

    Telemusic(workdir=Path(sys.argv[0]).parent.parent).run(
        create_tables=args.create_tables)
