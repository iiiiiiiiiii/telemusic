import asyncio
import logging

import mpv
from ..helpers.player import create_player

log = logging.getLogger("mpv")


def logger(level, prefix, text):
    log.debug(text)


class Chat:
    def __init__(self, chat_id, call, input_call, audio_device):
        self.chat_id = chat_id

        self.call = call
        self.input_call = input_call

        self.audio_device = audio_device
        self.proc = self._get_proc()

        self.player_msg = None
        self.player_task = None
        self.song = None

    async def stop_playback(self):
        await asyncio.get_event_loop().run_in_executor(None, self.proc.stop)

    async def terminate_process(self, proc=None):
        proc = proc or self.proc
        await asyncio.get_event_loop().run_in_executor(None, proc.terminate)

    async def _loop(self, proc=None):
        proc = proc or self.proc
        await asyncio.get_event_loop().run_in_executor(None, proc._loop)

    async def wait(self, proc=None):
        proc = proc or self.proc
        try:
            await asyncio.get_event_loop().run_in_executor(
                None, proc.wait_for_playback)
        except mpv.ShutdownError:
            pass

    async def stop(self):
        await self.call.stop()
        await self.terminate_process()

    async def skip(self):
        await self.stop_playback()
        await self.call.skip_track()

    async def _update_player(self):
        if not self.player_msg or not self.song:
            return False
        text, keyboard = create_player(self, self.chat_id)
        await self.player_msg.edit_text(text, reply_markup=keyboard,
                                        disable_web_page_preview=True)
        return True

    async def _update_player_loop(self, times=30, sleep=5):
        for i in range(times):
            try:
                ok = await self._update_player()
                if not ok:
                    break
                await asyncio.sleep(sleep - (self.time_pos % 1))
            except asyncio.CancelledError:
                break

    def update_player(self, times=30, sleep=5):
        if self.player_task and not self.player_task.done():
            self.player_task.cancel()
        self.player_task = asyncio.create_task(
                self._update_player_loop(times, sleep))

    @property
    def volume(self):
        return self.proc.volume

    def set_volume(self, n):
        self.proc.volume = n

    def volume_up(self, n):
        self.proc.volume += n

    def volume_down(self, n):
        self.proc.volume -= n

    def play(self, url):
        self.proc.play(url)

    async def play_in_parallel(self, url):
        loop = asyncio.get_event_loop()
        proc = self._get_proc()

        loop.create_task(self._loop(proc))
        proc.play(url)
        await self.wait(proc)
        await self.terminate_process(proc)

    def seek(self, n):
        try:
            self.proc.seek(n)
            return True
        except SystemError:
            return False

    @property
    def is_paused(self):
        return self.proc.pause

    def pause(self):
        self.proc.pause = True

    def resume(self):
        self.proc.pause = False

    @property
    def time_pos(self):
        t = self.proc.time_pos
        if not t:
            return 0

        if t < 0:
            t = 0
        if t > self.proc.duration:
            t = self.proc.duration
        return t

    def _get_proc(self):
        return mpv.MPV(start_event_thread=False,
                       audio_device=self.audio_device,
                       video="no",
                       log_handler=logger)
