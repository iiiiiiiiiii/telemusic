from ..constants import YOUTUBE_URL, YOUTUBE_EMBED_URL, YOUTUBE_THUMB_URL


class YoutubeVideo:
    def __init__(self, id,
                 title, description,
                 duration, view_count, uploader,
                 **kwargs):
        self.id = id
        self.title = title
        self.description = description
        self.duration = duration
        self.view_count = view_count
        self.uploader = uploader

    @property
    def url(self):
        return YOUTUBE_URL.format(self.id)
    
    @property
    def embed_url(self):
        return YOUTUBE_EMBED_URL.format(self.id)
    
    @property
    def thumb_url(self):
        return YOUTUBE_THUMB_URL.format(self.id)

    def __str__(self):
        return self.id
