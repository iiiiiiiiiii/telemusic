from pyrogram.types import InlineKeyboardMarkup, InlineKeyboardButton


class InlineKeyboardMarkupBuilder(InlineKeyboardMarkup):
    def __init__(self, inline_keyboard=None):
        inline_keyboard = inline_keyboard or [[]]
        super().__init__(inline_keyboard)

    def add_button(self, text,
                   callback_data=None, url=None, switch_inline_query=None,
                   switch_inline_query_current_chat=None):
        button = InlineKeyboardButton(
            text,
            callback_data=callback_data,
            url=url,
            switch_inline_query=switch_inline_query,
            switch_inline_query_current_chat=switch_inline_query_current_chat)
        self.inline_keyboard[-1].append(button)
        return self

    def new_row(self):
        self.inline_keyboard.append([])
        return self


class ActionBuilder:
    _volume_up = "vol_up"
    _volume_down = "vol_down"
    _pause = "pause"
    _resume = "resume"

    _forward = "forward"
    _backward = "backward"
    _double_forward = "Dforward"
    _double_backward = "Dbackward"

    _none = "_"

    def __init__(self, chat_id):
        self.chat_id = chat_id

    def __getattr__(self, attr):
        if attr != "none":
            return getattr(self, f"_{attr}") + f":{self.chat_id}"
        else:
            return "none"
