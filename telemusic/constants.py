EMPTY_SYMBOL = "\u200e"
EXEC_TIMEOUT = 20
MAX_VOLUME = 200

CONNECT_SOUND_PATH = "assets/connect.mp3"
DISCONNECT_SOUND_PATH = "assets/disconnect.mp3"
PING_SOUND_PATH = "assets/ping.mp3"

YOUTUBE_URL = "https://www.youtube.com/watch?v={}"
YOUTUBE_EMBED_URL = "https://www.youtube.com/embed/{}"
YOUTUBE_THUMB_URL = "https://img.youtube.com/vi/{}/hqdefault.jpg"
YDL_OPTIONS = {
    "format": "bestaudio,worstaudio,worstvideo,worst",
    "quiet": True,
    "extract_flat": "in_playlist"}

BAR_PLACEHOLDER = "\u2500"
BAR_CURSOR = "\u2b24"

FORWARD = "\u25ba"
BACKWARD = "\u25c4"
FAST_FORWARD = "\u25ba" * 2
FAST_BACKWARD = "\u25c4" * 2
VOLUME_UP = "\U0001f50a"
VOLUME_DOWN = "\U0001f509"

RESUME = "\u25B6"
PAUSE = "\u2590" * 2

# as emoji:
# BACKWARD = "\u25C0"
# FORWARD = "\u25B6"
# FAST_FORWARD = "\u23E9"
# FAST_BACKWARD = "\u23EA"
# RESUME = "\u25B6"

SEEK_VALUE = 5
VOLUME_VALUE = 5
MAX_PLAYER_LENGTH = 22
