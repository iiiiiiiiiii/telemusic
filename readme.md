# telemusic
bot for playing music in telegram group calls

## status
**WORK IN PROGRESS**

### working:
- queues
- searching videos on youtube
- skipping songs
- pausing
- changing volume
- interactive player
- seeking

### not working:
- multiple chats

### tgcalls module status
a dummy for future use

## setup
### requirements
python3.9
pip packages:
- pyrogram
- youtube-dl
- tortoise-orm
- python-mpv
- aiosqlite (or any other asyncio database implementation which supported by tortoise-orm)

desktop apps:
- mpv
- libmpv

### setting up environment
1. create python3 virtual environment
    ```sh
    python -m venv .venv
    ```
2. activate virtual environment
    ```sh
    source .venv/bin/activate
    ```
3. install requirements
    ```sh
    pip install -r requirements.txt
    ```
    install [mpv](https://mpv.io)
4. edit config

   copy config `telemusic.ini.example` to `telemusic.ini`

   as example, editing via your default terminal editor on unix-like systems, but you can use any other editor:
   ```sh
   $EDITOR telemusic.ini
   ```
   IMPORTANT!
   update `audio_device` to device which have loopback so you can forward it to telegram desktop.
   you can get list of devices with `mpv --audio-device=help`

   change `api_id` and `api_hash`. you can get them from my.telegram.org

   change `bot_token` to your bot token. you can get it from @BotFather

   also if you need change `db_url`
5. run 
    ```sh
    python -m telemusic
    ```
   on first run it will ask phone number

